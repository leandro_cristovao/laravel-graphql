<?php

namespace App\Repositories;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class BaseRepository
{
  /**
   * Model class
   *
   * @var string
   */
  protected $modelClass;

  function __construct($model)
  {
    $this->modelClass = $model;
  }

  public function upsert(array $args, GraphQLContext $context)
  {
    // $auth = $context->user();
    $entity = null;
    if (isset($args['input']['id'])) {
      $entity = $this->modelClass::findOrFail($args['input']['id']);
      $entity->fill($args['input']);
    } else {
      $entity = $this->modelClass::create($args['input']);
    }

    return $entity;
  }

  public function paginate($searchParam, GraphQLContext $context)
  {
    $perPage = $searchParam['pagination']['perPage'];
    $page = $searchParam['pagination']['page'];
    $page = ($page * $perPage) - $perPage;

    $Model = $this->modelClass::query();
    foreach ($searchParam['filters'] as $filter) {
      $field = $filter['field'];
      $operator = $filter['operator'];
      $value = $filter['value'];

      $Model->where($field, $operator, $value);
    }

    $total = $Model->count();

    foreach ($searchParam['orders'] as $order) {
      $field = $order['field'];
      $order = $order['order'];

      $Model->orderBy($field, $order);
    }

    $entities = $Model->skip($page)->take($perPage)->get();
    return [
      "pages" => [
        "total" => $total,
        "perPage" => $perPage,
        "page" => $page + 1,
        "lastPage" => 4
      ],
      "rows" =>  $entities
    ];
  }
}
