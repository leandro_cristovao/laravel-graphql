<?php

namespace App\GraphQL\Queries;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use \App\GraphQL\Utils;
use \App\Repositories\BaseRepository;

class QueryData
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args, GraphQLContext $context, $resolverInfo)
    {
        $fullClassName = "\App\Models\\{$resolverInfo->fieldName}";
        $model = new $fullClassName;

        $searchParam = $args['searchParam'];
        $repository = new BaseRepository($model);
        return $repository->paginate($searchParam, $context);
    }
}
