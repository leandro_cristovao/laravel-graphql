<?php

namespace App\GraphQL\Mutations;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use \App\GraphQL\Utils;
use \App\Repositories\BaseRepository;

class UpsertData
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke(
        $_,
        array $args,
        GraphQLContext $context,
        $resolverInfo
    ) {

        $className = substr($resolverInfo->fieldName, 6);
        $fullClassName = "\App\Models\\{$className}";
        $model = new $fullClassName;

        $repository = new BaseRepository($model);
        return $repository->upsert($args, $context);
    }
}
