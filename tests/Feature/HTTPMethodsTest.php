<?php

namespace Tests\Feature;

use Nuwave\Lighthouse\Testing\MakesGraphQLRequests;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class HTTPMethodsTest extends TestCase
{

    use MakesGraphQLRequests;
    use RefreshDatabase;

    /**
     * A basic feature test login.
     *
     * @return void
     */
    public function test_login_valido()
    {
        $response = $this->postJson('/api/login', [
            'email' => env('LOGIN'),
            'password' => env('PASSWORD')
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);
    }

    public function test_login_invalido()
    {
        $response = $this->postJson('/api/login', [
            'email' => 'admin@dotgroup.com.br',
            'password' => '12345678'
        ]);

        $response->assertStatus(401);
    }

    public function test_graphql_whithout_auth()
    {
        $response = $this->graphQL(
            /** @lang GraphQL */
            '
            {
                __schema {
                  types {
                    name
                  }
                }
              }
            '
        );

        $response->assertStatus(401);
    }

    public function test_graphql_auth()
    {
        $this->postJson('/api/login', [
            'email' => 'admin@dotgroup.com.br',
            'password' => '123456'
        ])->decodeResponseJson();
        // $token = $token['token'];


        $response = $this
            ->graphQL(
                /** @lang GraphQL */
                '
            {
                __schema {
                  types {
                    name
                  }
                }
              }
            '
            );

        $response->assertStatus(200);
    }
}
