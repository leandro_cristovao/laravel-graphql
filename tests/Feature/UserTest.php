<?php

namespace Tests\Feature;

use Nuwave\Lighthouse\Testing\MakesGraphQLRequests;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

class UserTest extends TestCase
{
  use RefreshDatabase;
  use MakesGraphQLRequests;

  protected $model = User::class;

  public function test_list_users()
  {

    $this->postJson('/api/login', [
      'email' => 'admin@dotgroup.com.br',
      'password' => '123456'
    ]);

    $response = $this
      ->graphQL(
        /** @lang GraphQL */
        '
                {
                    User(searchParam: {
                      filters: [
                        {
                          field: "id"
                          operator: "="
                          value: "1"
                        }
                         {
                          field: "email"
                          operator: "like"
                          value: "%dotgroup%"
                        }
                      ]
                      orders: [
                        {
                          field: "id"
                          order: DESC
                        }
                      ]
                      pagination: {
                        page: 1
                        perPage: 5
                      }
                    }) {
                      pages {
                        total
                        lastPage
                        page
                        perPage
                      }
                      rows {
                        id
                        name
                        email
                        created_at
                        updated_at
                      }
                    }
                  }                 
            '
      );

    $response->assertStatus(200);
  }

  public function test_list_upsertUser_create()
  {
    $this->postJson('/api/login', [
      'email' => 'admin@dotgroup.com.br',
      'password' => '123456'
    ]);


    $response = $this
      ->graphQL(
        /** @lang GraphQL */
        '
                mutation {
                    upsertUser(input: {
                      id: 2
                      name: "Jośe da Silva"
                      email: "teste@terra.com.br", 
                      password: "123456"
                    }) {
                      id
                      name
                      email
                    }
                  }                                
            '
      );

    $response->assertStatus(200);
  }

  public function test_list_upsertUser_update()
  {
    // factory(User::class)->create(['name' => 'Oliver']);

    $this->postJson('/api/login', [
      'email' => 'admin@dotgroup.com.br',
      'password' => '123456'
    ]);


    $response = $this
      ->graphQL(
        /** @lang GraphQL */
        '
                mutation {
                    upsertUser(input: {
                      id: 2
                      name: "Jośe da Silva"
                      email: "teste@terra.com.br", 
                      password: "123456"
                    }) {
                      id
                      name
                      email
                    }
                  }                                
            '
      );

    $response->assertStatus(200);
  }
}
