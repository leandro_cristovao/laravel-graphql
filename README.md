Core Laravel 8 + GraphQL + MySQL + Unit Tests + Repository Pattern

A ferramenta lighthouse (https://lighthouse-php.com/) que fornece o uso do graphql e uma integração robusta com o Laravel tem seus méritos. Neste skelenton optei em usar o mínimo de recursos da ferramenta para implementar de forma unificada os métodos de CRUD de forma genérica usando Repository.

# Instalação

## Requisitos para instalação

    git
    php 7.x
    php-xml
    php-mysql
    composer

    docker
    docker-compose

## Configuração

Criar dois bancos de dados

    create database laravel CHARACTER SET utf8 COLLATE utf8_general_ci;
    create database laravel_test CHARACTER SET utf8 COLLATE utf8_general_ci;

Renomeie o arquivo `.env.example` para `.env`, ajuste os as configurações de acesso ao banco de dados.

Rodar os comandos

    composer install
    php artisan migrate
    php artisan db:seed

Parar rodar a aplicação, executar `php artisan serve`

## Executando no Docker

Rodar os comandos

    php artisan sail:install
    ./vendor/bin/sail up

Maiores informações em https://laravel.com/docs/8.x/sail

# Testes Unitários

Para rodar os testes, executar (executar `mkdir tests/Unit` caso a pasta não exista)

    php artisan test

Para rodar um único arquivo de teste, executar

    php artisan test --filter=UserTest

Para gerar o relatório de cobertura de código, executar

    php artisan test --coverage-html reports/

# Referências

    https://lighthouse-php.com/
    https://www.toptal.com/graphql/laravel-graphql-server-tutorial
