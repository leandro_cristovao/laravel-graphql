<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        $password = bcrypt('123456');
        User::create([
            'name'     => 'admin',
            'email'    => 'admin@dotgroup.com.br',
            'password' => $password,
        ]);
    }
}
